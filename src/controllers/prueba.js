const api= require("express");
const cheerio= require("cheerio");
const request= require("request-promise");
const TrmApi = require('trm-api').default;
const trmapi = new TrmApi();
var meli = require('mercadolibre-nodejs-sdk');
var cron = require('node-cron');
const app= api();


var amazon;
//Controlador //

const postAmazonMercadolibre= async (req,res) => {
    const{sku}=req.body;
 try{
   amazon=await request('https://www.amazon.com/-/dp/'+sku);
   
 }catch(error){
   res.status(301).json({error:'error en obtener la pagina'});
  }

  const pagina= await obtenerPagina();
  crearPost(pagina);
  tarea.start();

  
}


async function obtenerPagina(){
const $li = cheerio.load(amazon);
var titulo=($li('span[id="productTitle"]').text().trim());
var precio=($li('span[id="priceblock_ourprice"]').text().trim());
var precios=precio.split('');
console.log(precios[0]);
if(precios[0]=='$'){
    const aux=precio.replace('$', ''); 
let peso= await trmapi.latest().then((data)=> data).catch((error) => console.log(error));   
 precio=aux*peso.valor;
}else{
   precio=precio.replace('COP', ''); 
}
return {titulo,precio};
}

function crearPost(pagina){
   const InlineObject = {
      title:pagina['titulo'],
      category_id:"MCO441869",
      price:Math.round(pagina['precio']),
      currency_id:"COP",
      available_quantity:10,
      buying_mode:"buy_it_now",
      condition:"new",
      listing_type_id:"gold_special",
      description:{
         "plain_text":"Descripción con Texto Plano \n"
      },
      video_id:"YOUTUBE_ID_HERE",
      sale_terms:[
         {
            "id":"WARRANTY_TYPE",
            "value_name":"Garantía del vendedor"
         },
         {
            "id":"WARRANTY_TIME",
            "value_name":"90 días"
         }
      ],
      pictures:[
         {
            "source":"http://mla-s2-p.mlstatic.com/968521-MLA20805195516_072016-O.jpg"
         }
      ],
      attributes:[
         {
            "id":"BRAND",
            "value_name":"Marca del producto"
         },
         {
            "id":"EAN",
            "value_name":"7898095297749"
         }
      ],
      accepts_mercadopago: true,
      shipping: {
         "mode": "me2",
         "local_pick_up": false,
         "free_shipping": false,
         "free_methods": []
      }
  
};
     let apiInstance = new meli.RestClientApi();
     let resource = 'items'; // Example "items" |
     let accessToken = "APP_USR-2344449426683321-050700-520084e6459a6f69ec815fecfe5a7f1d-744449299"; 
     let body = InlineObject; // Object |
     apiInstance.resourcePost(
       resource,
       accessToken,
       body,
       (error, data, response) => {
         if (error) {
           console.error(error);
         } else {
           console.log('Grabado con exito.');
         }
       }
     );
}






const tarea= cron.schedule('0 0 */8 * *', () =>  {
    console.log("******* CADA 8 HORAS TRM********");
     trmapi.latest().
     then((data)=> console.log(data)).
     catch((error) => console.log(error));   
  }, {
    scheduled: false
  });


module.exports=postAmazonMercadolibre;